package com.example.ido.exc2;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {
    private final int INITIAL_CHARACTER = 2;

    int boardMatrix[][];
    List<String> players;
    List<Button> buttons = new ArrayList<>();
    int currentPlayerIndex;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        players = new ArrayList<>();
        players.add("X");
        players.add("O");
        currentPlayerIndex = 0;
        setBoard();
    }

    // Set up the game board.
    private void setBoard() {
        boardMatrix = new int[3][3];

        textView = (TextView) findViewById(R.id.txtTurn);

        initializeClickListeners();

        for (int i = 0; i <= boardMatrix.length - 1; i++) {
            for (int j = 0; j <= boardMatrix.length - 1; j++)
                boardMatrix[i][j] = INITIAL_CHARACTER;
        }

        textView.setText(players.get(currentPlayerIndex) + " Turn");
    }

    private void initializeClickListeners() {
        Button b[][] = new Button[3][3];

        b[0][2] = (Button) findViewById(R.id.button1);
        b[0][1] = (Button) findViewById(R.id.button2);
        b[0][0] = (Button) findViewById(R.id.button3);

        b[1][2] = (Button) findViewById(R.id.button4);
        b[1][1] = (Button) findViewById(R.id.button5);
        b[1][0] = (Button) findViewById(R.id.button6);

        b[2][2] = (Button) findViewById(R.id.button7);
        b[2][1] = (Button) findViewById(R.id.button8);
        b[2][0] = (Button) findViewById(R.id.button9);

        // add the click listeners for each button
        for (int i = 0; i <= 2; i++) {
            for (int j = 0; j <= 2; j++) {
                b[i][j].setOnClickListener(new MyClickListener(i, j));
                b[i][j].setBackground(getResources().getDrawable(R.mipmap.empty_cell));

                if (!b[i][j].isEnabled()) {
                    b[i][j].setEnabled(true);
                }
                buttons.add(b[i][j]);
            }
        }
    }

    private String checkBoard() {
        String gameStatus = "normal";

        // horizontal line
        if (boardMatrix[0][0] == boardMatrix[0][1] && boardMatrix[0][0] == boardMatrix[0][2] && boardMatrix[0][2] != INITIAL_CHARACTER)
            gameStatus = "victory";
        else if (boardMatrix[1][0] == boardMatrix[1][1] && boardMatrix[1][0] == boardMatrix[1][2] && boardMatrix[1][2] != INITIAL_CHARACTER)
            gameStatus = "victory";
        else if (boardMatrix[2][0] == boardMatrix[2][1] && boardMatrix[2][0] == boardMatrix[2][2] && boardMatrix[2][2] != INITIAL_CHARACTER)
            gameStatus = "victory";
        // vertical line
        else if (boardMatrix[0][0] == boardMatrix[1][0] && boardMatrix[0][0] == boardMatrix[2][0] && boardMatrix[2][0] != INITIAL_CHARACTER)
            gameStatus = "victory";
        else if (boardMatrix[0][1] == boardMatrix[1][1] && boardMatrix[0][1] == boardMatrix[2][1] && boardMatrix[2][1] != INITIAL_CHARACTER)
            gameStatus = "victory";
        else if (boardMatrix[0][2] == boardMatrix[1][2] && boardMatrix[0][2] == boardMatrix[2][2] && boardMatrix[1][2] != INITIAL_CHARACTER)
            gameStatus = "victory";
        // Diagonal line
        else if (boardMatrix[0][0] == boardMatrix[1][1] && boardMatrix[1][1] == boardMatrix[2][2] && boardMatrix[2][2] != INITIAL_CHARACTER)
            gameStatus = "victory";
        // Second diagonal line
        else if (boardMatrix[0][2] == boardMatrix[1][1] && boardMatrix[0][2] == boardMatrix[2][0] && boardMatrix[2][0] != INITIAL_CHARACTER)
            gameStatus = "victory";
        // Check if tie
        else {
            boolean isTie = true;
            for (int i = 0; i <= boardMatrix.length - 1; i++) {
                for (int j = 0; j <= boardMatrix.length - 1; j++)
                    if (boardMatrix[i][j] == INITIAL_CHARACTER) {
                        isTie = false;
                        break;
                    }
            }

            if (isTie) {
                gameStatus = "tie";
            }
        }

        return gameStatus;
    }

    private void disableBoard() {
        for (Button button : buttons) {
            button.setEnabled(false);
        }
    }

    class MyClickListener implements View.OnClickListener {
        int x;
        int y;

        public MyClickListener(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public void onClick(View view) {
            Button clickedButton = (Button) view;
            if (clickedButton.isEnabled()) {
                clickedButton.setEnabled(false);
                clickedButton.setBackground(getImageForPlayer(players.get(currentPlayerIndex)));
                boardMatrix[x][y] = currentPlayerIndex;

                switch (checkBoard()) {
                    case "victory": {
                        textView.setText(players.get(currentPlayerIndex) + " WON!!!");
                        disableBoard();
                        break;
                    }
                    case "tie": {
                        textView.setText("It's a tie!");
                        disableBoard();
                        break;
                    }
                    case "normal":
                    default: {
                        currentPlayerIndex = (currentPlayerIndex + 1) % 2;
                        textView.setText(players.get(currentPlayerIndex) + " Turn");
                        break;
                    }
                }
            }
        }
    }

    private Drawable getImageForPlayer(String player) {
        int drawableId;
        switch (player) {
            case "X": {
                drawableId = R.mipmap.x;
                break;
            }
            case "O":
            default: {
                drawableId = R.mipmap.circle;
                break;
            }
        }

        return getResources().getDrawable(drawableId);
    }
}

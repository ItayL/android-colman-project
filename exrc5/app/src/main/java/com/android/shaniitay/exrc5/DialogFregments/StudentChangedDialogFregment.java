package com.android.shaniitay.exrc5.DialogFregments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.app.DialogFragment;

import com.android.shaniitay.exrc5.Model.Ride;
import com.android.shaniitay.exrc5.R;

public abstract class StudentChangedDialogFregment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String dialogMessage = getDialogMessage();
        builder.setMessage(dialogMessage)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) { }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    @NonNull
    protected abstract String getDialogMessage();

    public Ride getSavedStudent() {
        return _savedRide;
    }

    public void setStudent(Ride savedRide) {
        this._savedRide = savedRide;
    }

    private Ride _savedRide;
}

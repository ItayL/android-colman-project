package com.android.shaniitay.exrc5.Model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.android.shaniitay.exrc5.MainActivity;
import com.android.shaniitay.exrc5.MyApplication;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Shani on 18/06/2017.
 */

public class UserSql {
    final public static UserSql Instance = new UserSql();
    private SqLightDB _dbInstance;
    private static final String
            USER_TABLE = "user_table",
            ID_COL = "id",
            NAME_COL = "name";
    private static final String
            RIDE_PASSENGERS_TABLE = "ride_passengers_table",
            RIDE_ID_COL = "ride_id",
            USER_ID_COL = "user_id";


    public UserSql() {
        this._dbInstance = new SqLightDB(MyApplication.getMyContext());
    }

    public void resetUsersTable(SQLiteDatabase db) {
        try {
            db.execSQL("drop table " + USER_TABLE + ";");
            db.execSQL("drop table " + RIDE_PASSENGERS_TABLE + ";");
        } catch (Exception ex) {

        }
        createUsersTable(db);
        createRidePassengersTable(db);
    }


    public void createUsersTable(SQLiteDatabase db) {
        String usersTableCreateCommand =
                "create table " + USER_TABLE + " (" +
                        ID_COL + " TEXT PRIMARY KEY, " +
                        NAME_COL + " TEXT);";

        db.execSQL(usersTableCreateCommand);
    }

    public void createRidePassengersTable(SQLiteDatabase db) {
        String usersTableCreateCommand =
                "create table " + RIDE_PASSENGERS_TABLE + " (" +
                        USER_ID_COL + " TEXT, " +
                        RIDE_ID_COL + " TEXT," +
                        "PRIMARY KEY (" + USER_ID_COL + "," + RIDE_ID_COL + "));";

        db.execSQL(usersTableCreateCommand);
    }

    public User getUser(String id) {
        try (SQLiteDatabase dbRead = _dbInstance.getReadableDatabase()) {
            String whereStr = ID_COL + "='" + id + "'";
            Cursor cursor = dbRead.query(USER_TABLE, null, whereStr, null, null, null, null);
            HashMap<String, User> users = readCursorToUsersList(cursor);

            if (users.isEmpty())
                return null;
            else
                return users.get(id);
        }
    }

    public void addPassenger(String userId, String rideId) {
        try (SQLiteDatabase dbWrite = _dbInstance.getWritableDatabase()) {
            ContentValues cv = new ContentValues();
            cv.put(USER_ID_COL, userId);
            cv.put(RIDE_ID_COL, rideId);
            dbWrite.replace(RIDE_PASSENGERS_TABLE, ID_COL, cv);
        }
    }

    public Map<String, User> getPassengers(String rideId) {
        try (SQLiteDatabase dbRead = _dbInstance.getReadableDatabase()) {
            String query = "SELECT " + ID_COL + ", " + NAME_COL +
                    " FROM " + USER_TABLE + ", " + RIDE_PASSENGERS_TABLE +
                    " WHERE " + USER_ID_COL + " = " + ID_COL + " AND " + RIDE_ID_COL +
                    "=?";
            Cursor cursor = dbRead.rawQuery(query, new String[]{rideId});

            return readCursorToUsersList(cursor);
        }
    }

    private HashMap<String, User> readCursorToUsersList(Cursor cursor) {
        HashMap<String, User> retList = new HashMap<>();

        while (cursor.moveToNext()) {
            ContentValues rowCV = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(cursor, rowCV);
            String userId = rowCV.getAsString(ID_COL);
            String userName = rowCV.getAsString(NAME_COL);
            User user = new User(userId, userName);
            retList.put(user.getId(), user);
        }
        return retList;
    }

    public void addUser(User user) {
        try (SQLiteDatabase dbWrite = _dbInstance.getWritableDatabase()) {
            ContentValues cv = new ContentValues();
            cv.put(ID_COL, user.id);
            cv.put(NAME_COL, user.name);
            dbWrite.replace(USER_TABLE, ID_COL, cv);
        }
    }
}

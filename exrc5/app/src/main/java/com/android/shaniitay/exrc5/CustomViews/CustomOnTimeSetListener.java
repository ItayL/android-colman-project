package com.android.shaniitay.exrc5.CustomViews;

public interface CustomOnTimeSetListener {
    void onTimeSet(int hour, int min);
}

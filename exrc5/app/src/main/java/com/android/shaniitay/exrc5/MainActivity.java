package com.android.shaniitay.exrc5;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.android.shaniitay.exrc5.Filter.FilterConditions;
import com.android.shaniitay.exrc5.Model.Model;
import com.android.shaniitay.exrc5.Model.Ride;

public class MainActivity extends FragmentActivity implements RidesListFragment.OnFragmentInteractionListener,
        RideDetailsFragment.OnFragmentInteractionListener,
        NewRideFragment.OnFragmentInteractionListener,
        EditRideFragment.OnFragmentInteractionListener,
        FilterFragment.OnFilterInteraction{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        _context = getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        if(savedInstanceState != null)
            return;
        actionBar.setTitle("Test");

        Fragment ridesListFragment = RidesListFragment.newRidestListFragment(null, "Rides List");

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.main_activity, ridesListFragment);
        fragmentTransaction.addToBackStack("rides_list");
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 1)
            getFragmentManager().popBackStack();
        else if (getFragmentManager().getBackStackEntryCount() == 1)
            finish();
        else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.btnLogout:
                AuthenticationActivity.logOutAndRedirectHere(this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRideSelected(String rideId) {
        Fragment rideDetailsFragment = RideDetailsFragment.newRideDetailsFragment(rideId);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_activity, rideDetailsFragment).addToBackStack("ride_details");
        fragmentTransaction.commit();
    }

    @Override
    public void onNewRidePressed() {
        Fragment newRideFragment = NewRideFragment.newNewRideFragment();

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_activity, newRideFragment).addToBackStack("new_ride");
        fragmentTransaction.commit();
    }

    @Override
    public void onFilterPressed() {
        Fragment filterFragment = FilterFragment.newFilterFragment();

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_activity, filterFragment).addToBackStack("filter");
        fragmentTransaction.commit();
    }

    @Override
    public void onMyRidesPressed() {
        FilterConditions filterConditions = new FilterConditions();
        filterConditions.driver = AuthenticationActivity.getCurrentLoginUser().getId();
        Fragment rideListFragment = RidesListFragment.newRidestListFragment(filterConditions, "My Rides");

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_activity, rideListFragment).addToBackStack("rides_list_my_rides");
        fragmentTransaction.commit();
    }

    @Override
    public void onUpdateRidePressed(String rideId) {
        Fragment editRideFragment = EditRideFragment.newEditRideFragment(rideId);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_activity, editRideFragment).addToBackStack("edit_ride");
        fragmentTransaction.commit();
    }

    @Override
    public void onNewRideSaved(String rideId) {
        getFragmentManager().popBackStack();
    }

    @Override
    public void onNewRideCanceled() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void onRideSaved(String id) {
        getFragmentManager().popBackStack();
    }

    @Override
    public void onRideDeleted(String id) {
        getFragmentManager().popBackStack("ride_details", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onEditCanceled(String id) {
        getFragmentManager().popBackStack();
    }

    public static Context getAppContext() {
        return _context;
    }
    private static Context _context;

    @Override
    public void onFilterPressed(FilterConditions filterConditions) {
        Fragment rideListFragment = RidesListFragment.newRidestListFragment(filterConditions, "Filter Results");

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_activity, rideListFragment).addToBackStack("rides_list_filter");
        fragmentTransaction.commit();
    }

    @Override
    public void onFilterCanceled() {
        getFragmentManager().popBackStack();
    }
}

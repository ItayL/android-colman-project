package com.android.shaniitay.exrc5.Model;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.android.shaniitay.exrc5.MyApplication;

/**
 * Created by itay on 6/12/2017.
 */

public class SqLightDB extends SQLiteOpenHelper {
    SqLightDB(Context context) {
        super(context, "students_app_database.db", null, 23);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        RideSql.Instance.createRidesTable(db);
        UserSql.Instance.createUsersTable(db);
        UserSql.Instance.createRidePassengersTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        RideSql.Instance.resetRidesTable(db);
        UserSql.Instance.resetUsersTable(db);
        // set update date to 0 in order to get all rides from remote DB
        SharedPreferences.Editor prefEd = MyApplication.getMyContext().getSharedPreferences("TAG",
                Context.MODE_PRIVATE).edit();
        prefEd.putFloat(Model.RIDES_LAST_UPDATE_DATE_PREF_KEY, (float) 0);
        prefEd.commit();

    }
}

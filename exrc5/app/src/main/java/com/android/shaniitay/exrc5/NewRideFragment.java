package com.android.shaniitay.exrc5;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.MediaStore;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.shaniitay.exrc5.Model.InputFieldsValidatior;
import com.android.shaniitay.exrc5.Model.Model;
import com.android.shaniitay.exrc5.Model.Ride;
import com.android.shaniitay.exrc5.CustomViews.CustomDatePicker;
import com.android.shaniitay.exrc5.CustomViews.CustomTimePicker;

import java.io.ByteArrayOutputStream;
import java.sql.Time;
import java.util.Calendar;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewRideFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewRideFragment#newNewRideFragment} factory method to
 * create an instance of this fragment.
 */
public class NewRideFragment extends Fragment {
    private static final String ARG_BITMAP = "bitmap";

    private OnFragmentInteractionListener mListener;
    private EditText _sourceEt;
    private EditText _destinationEt;
    private CustomTimePicker _rideTimePicker;
    private CustomDatePicker _rideDatePicker;
    private EditText _phoneEt;
    private EditText _numberOfPlacesEt;
    ImageView imageView;
    Bitmap imageBitmap;
    private View _rideCreationLoadingAnimationView;
    private Calendar _selectedDateCalendar;
    private String _newRideId;
    private static String LAST_SELECTED_CALENDAR = "LAST_SELECTED_CALENDAR";
    private View _saveBtn;

    public NewRideFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NewRideFragment.
     */
    public static NewRideFragment newNewRideFragment() {
        NewRideFragment fragment = new NewRideFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_ride, container, false);

        _newRideId = UUID.randomUUID().toString();

        findAndInitViews(view, savedInstanceState);
        assignBtnListeners(view, savedInstanceState);
        getActivity().getActionBar().setTitle("New Ride");

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (imageBitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            outState.putByteArray(ARG_BITMAP, byteArray);
        }

        outState.putLong(LAST_SELECTED_CALENDAR, _selectedDateCalendar.getTime().getTime());
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFilterInteraction");
        }
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFilterInteraction");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void assignBtnListeners(View view, Bundle savedInstanceState) {
        final Button save = (Button) view.findViewById(R.id.main_save_btn);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!InputFieldsValidatior.validateFields(_numberOfPlacesEt, _sourceEt, _destinationEt, _phoneEt, getActivity()))
                    return;

                save.setEnabled(false);
                _rideCreationLoadingAnimationView.setVisibility(View.VISIBLE);

                final Ride st = new Ride(_sourceEt.getText().toString(),
                        _destinationEt.getText().toString(),
                        _newRideId,
                        _phoneEt.getText().toString(),
                        _selectedDateCalendar.getTime(),
                        Integer.parseInt(_numberOfPlacesEt.getText().toString()),
                        "",
                        AuthenticationActivity.getCurrentLoginUser());

                if (imageBitmap == null) {
                    addNewRideToDB(st);
                } else {
                    Model.instance().saveImage(imageBitmap, st.id + ".jpeg", new Model.SaveImageListener() {
                        @Override
                        public void complete(String url) {
                            st.imageUrl = url;
                            addNewRideToDB(st);
                        }

                        @Override
                        public void fail() {
                            //notify operation fail,...
                            _rideCreationLoadingAnimationView.setVisibility(GONE);
                            _saveBtn.setEnabled(true);
                            ToastsUtils.raiseToast(getActivity(), "Error Saving Image..");
                        }
                    });
                }
            }
        });

        Button cancel = (Button) view.findViewById(R.id.main_cancel_btn);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onNewRideCanceled();
            }
        });

        imageView = (ImageView) view.findViewById(R.id.new_ride_image_view);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        if (savedInstanceState != null && savedInstanceState.containsKey(ARG_BITMAP)) {
            byte[] byteArray = savedInstanceState.getByteArray(ARG_BITMAP);
            imageBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        }
        if (imageBitmap != null) {
            imageView.setImageBitmap(imageBitmap);
        }
    }

    private void addNewRideToDB(Ride st) {
        Model.instance().addRide(st, new Model.AddRideListener() {
            @Override
            public void complete(String newRideId) {
                _rideCreationLoadingAnimationView.setVisibility(GONE);
                _saveBtn.setEnabled(true);
                mListener.onNewRideSaved(_newRideId);
            }

            @Override
            public void fail(String newRideId) {
                _rideCreationLoadingAnimationView.setVisibility(GONE);
                _saveBtn.setEnabled(true);
                ToastsUtils.raiseToast(getActivity(), "Error Saving New Ride..");
            }
        });
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
        }
    }

    private void findAndInitViews(View view, Bundle savedInstanceState) {
        _sourceEt = (EditText) view.findViewById(R.id.tv_ride_source);
        _destinationEt = (EditText) view.findViewById(R.id.tv_ride_destination);
        _phoneEt = (EditText) view.findViewById(R.id.main_phone_tv);
        _numberOfPlacesEt = (EditText) view.findViewById(R.id.new_ride_places_number);
        _rideTimePicker = (CustomTimePicker) view.findViewById(R.id.ride_time_picker);
        _rideDatePicker = (CustomDatePicker) view.findViewById(R.id.ride_date_picker);

        _selectedDateCalendar = Calendar.getInstance();
        if (savedInstanceState != null) {
            long selectedTime = savedInstanceState.getLong(LAST_SELECTED_CALENDAR);
            if (selectedTime != 0) {
                _selectedDateCalendar.setTime(new Time(selectedTime));
            }
        }

        _rideTimePicker.setSelectedTime(_selectedDateCalendar);
        _rideDatePicker.setSelectedDate(_selectedDateCalendar);

        _rideCreationLoadingAnimationView = view.findViewById(R.id.new_ride_loading_animation);
        _saveBtn = view.findViewById(R.id.main_save_btn);
        _rideCreationLoadingAnimationView.setVisibility(GONE);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onNewRideSaved(String rideId);

        void onNewRideCanceled();
    }
}

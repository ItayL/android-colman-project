package com.android.shaniitay.exrc5.Model;

import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ride implements Serializable {

    public String source;
    public String destination;
    public String id;
    public String phone;
    public String imageUrl;
    public long rideDate;
    public Map<String, User> passengers;
    public int numberOfPlaces;
    public double lastUpdateDate;
    public User creator;

    public Ride() {
        passengers = new HashMap<>();
    }

    public Ride(String source,
                String destination,
                String id,
                String phone,
                Date rideDate,
                int numberOfPlaces,
                String imageUrl,
                User creator) {
        this.source = source;
        this.destination = destination;
        this.id = id;
        this.phone = phone;
        this.rideDate = rideDate.getTime();
        this.numberOfPlaces = numberOfPlaces;
        this.passengers = new HashMap<>();
        this.imageUrl = imageUrl;
        this.creator = creator;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getNumberOfPlaces() {
        return numberOfPlaces;
    }

    public void setNumberOfPlaces(int numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

    public Map<String, User> getPassengers() {
        return passengers;
    }

    public void setPassengers(Map<String, User> passengers) {
        this.passengers = passengers;
    }

    public void addPassenger(User user) {
        this.passengers.put(user.getId(), user);
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public String getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public Time getRideDate() {
        return new Time(rideDate);
    }

    public void setRideDate(Long rideDate) {
        this.rideDate = rideDate;
    }
}
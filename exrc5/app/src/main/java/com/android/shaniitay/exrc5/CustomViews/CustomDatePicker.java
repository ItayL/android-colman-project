package com.android.shaniitay.exrc5.CustomViews;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;
import java.util.Date;

public class CustomDatePicker extends EditText implements CustomOnDateSetListener {
    private Calendar _selectedDateCalender = Calendar.getInstance();

    public CustomDatePicker(Context context) {
        super(context);
        setInputType(0);
        rebindUiToSelectedValues();
    }

    public CustomDatePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        setInputType(0);
        rebindUiToSelectedValues();
    }

    public CustomDatePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setInputType(0);
        rebindUiToSelectedValues();
    }

    public void setSelectedDate(Calendar calendar) {
        _selectedDateCalender = calendar;
        rebindUiToSelectedValues();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        if (event.getAction() == MotionEvent.ACTION_DOWN){
            CustomDatePickerDialog tpd =
                CustomDatePickerDialog.newInstance(getId(), _selectedDateCalender);
            tpd.show(((Activity)getContext()).getFragmentManager(), "TAG");
            return true;
        }
        return true;
    }

    @Override
    public void onDateSet(int year, int month, int dayOfMonth) {
        _selectedDateCalender.set(Calendar.YEAR, year);
        _selectedDateCalender.set(Calendar.MONTH, month);
        _selectedDateCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        rebindUiToSelectedValues();
    }

    private void rebindUiToSelectedValues() {
        String displayDate =
            DateTimeDisplayUtils.getDisplayDate(_selectedDateCalender);
        setText(displayDate);
        setTextSize(16);
    }

    public static class CustomDatePickerDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        private static final String TAG_ARG_KEY = "TAG_ARG_KEY";
        private static final String YEAR_ARG_KEY = "YEAR_ARG_KEY";
        private static final String MONTH_ARG_KEY = "MONTH_ARG_KEY";
        private static final String DAY_ARG_KEY = "DAY_ARG_KEY";
        CustomOnDateSetListener listener;

        public static CustomDatePickerDialog newInstance(int tag, Calendar calendar) {
            CustomDatePickerDialog timePickerDialog = new CustomDatePickerDialog();
            Bundle args = new Bundle();
            args.putInt(TAG_ARG_KEY, tag);
            args.putInt(YEAR_ARG_KEY, calendar.get(Calendar.YEAR));
            args.putInt(MONTH_ARG_KEY, calendar.get(Calendar.MONTH));
            args.putInt(DAY_ARG_KEY, calendar.get(Calendar.DAY_OF_MONTH));
            timePickerDialog.setArguments(args);
            return timePickerDialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            super.onCreateDialog(savedInstanceState);
            int year = getArguments().getInt(YEAR_ARG_KEY);
            int month = getArguments().getInt(MONTH_ARG_KEY);
            int dayOfMonth = getArguments().getInt(DAY_ARG_KEY);
            Dialog timePicker = new DatePickerDialog(getActivity(), this, year, month, dayOfMonth);

            if (getArguments() != null) {
                int tag = getArguments().getInt(TAG_ARG_KEY);
                listener = (CustomOnDateSetListener) getActivity().findViewById(tag);
            }

            return timePicker;
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            outState.putString("message", "This is my message to be reloaded");
            super.onSaveInstanceState(outState);
        }

        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
            if (listener != null) {
                listener.onDateSet(year, month, dayOfMonth);
            }
        }
    }
}
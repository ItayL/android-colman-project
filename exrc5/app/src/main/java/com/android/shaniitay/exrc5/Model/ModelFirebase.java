package com.android.shaniitay.exrc5.Model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.sql.Array;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Shani on 15/06/2017.
 */

public class ModelFirebase {

    public void getRide(String stId, final GetRideCallback callback) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("rides");
        myRef.child(stId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Ride ride = dataSnapshot.getValue(Ride.class);
                callback.onComplete(ride);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onCancel();
            }
        });
    }

    public void listenToRidesUpdates(final RideChangesListener listener) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ridesReference = database.getReference("rides");
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Ride newRide = dataSnapshot.getValue(Ride.class);
                listener.onRideAdded(newRide);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Ride updatedRide = dataSnapshot.getValue(Ride.class);
                listener.onRideUpdated(updatedRide);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String rideId = dataSnapshot.getKey();
                listener.onRideDeleted(rideId);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        ridesReference.addChildEventListener(childEventListener);
    }

    public void listenToUserUpdates(final UserChangesListener listener) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ridesReference = database.getReference("users");
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                User user = dataSnapshot.getValue(User.class);
                listener.onUserAdded(user);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String userId = dataSnapshot.getKey();
                listener.onUserDeleted(userId);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        ridesReference.addChildEventListener(childEventListener);
    }

    public Task<Void> addRide(final Ride ride) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("rides");
        Map<String, Object> value = new HashMap<>();
        value.put("id", ride.getId());
        value.put("source", ride.getSource());
        value.put("destination", ride.getDestination());
        value.put("numberOfPlaces", ride.getNumberOfPlaces());
        value.put("rideDate", ride.getRideDate().getTime());
        value.put("passengers", ride.getPassengers());
        value.put("phone", ride.getPhone());
        value.put("imageUrl", ride.getImageUrl());
        value.put("lastUpdateDate", ServerValue.TIMESTAMP);
        value.put("creator", ride.getCreator());
        Task<Void> rideCreationTask = myRef.child(ride.getId()).setValue(value);

        return rideCreationTask;
    }


    public Task<Void> addUser(User user) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("users");
        Map<String, Object> value = new HashMap<>();
        value.put("id", user.getId());
        value.put("name", user.getName());
        value.put("lastUpdateDate", ServerValue.TIMESTAMP);
        Task<Void> addUserTask = myRef.child(user.getId()).setValue(value);
        return addUserTask;
    }

    public Task<Void> addPassengerToRide(String rideId, User user) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("rides");
        Task<Void> passengersAddTask = myRef.child(rideId).child("passengers").push().setValue(user);

        return passengersAddTask;
    }

    public Task<Void> updateRide(String id,
                           String source,
                           String destination,
                           String phone,
                           Date rideDate,
                           Map<String, User> passengers,
                           int numberOfPlaces,
                           String imageUrl, User creator) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("rides");
        Map<String, Object> value = new HashMap<>();
        value.put("source", source);
        value.put("destination", destination);
        value.put("numberOfPlaces", numberOfPlaces);
        value.put("rideDate", rideDate.getTime());
        value.put("phone", phone);
        value.put("imageUrl", imageUrl);
        value.put("lastUpdateDate", ServerValue.TIMESTAMP);
        final Task<Void> ridesUpdateTask = myRef.child(id).updateChildren(value);
        final Task<Void> passengersUpdateTask = myRef.child(id).child("passengers").setValue(passengers);
        final Task<Void> creatorsUpdateTask = myRef.child(id).child("creator").setValue(creator);

//        Tasks.whenAll(new ArrayList<Task<?>>() {{
//            add(ridesUpdateTask);
//            add(passengersUpdateTask);
//            add(creatorsUpdateTask);
//        }});

        Task<Void> updateProcessTask = Tasks.whenAll(Arrays.asList(ridesUpdateTask, passengersUpdateTask, creatorsUpdateTask));
        return updateProcessTask;
    }

    public Task<Void> delete(Ride ride) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("rides");
        Task<Void> rideDeleteTask = myRef.child(ride.getId()).removeValue();

        return rideDeleteTask;
    }

    public void getAllRidesAndObserve(final GetAllRidesAndObserveCallback callback) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("rides");
        ValueEventListener listener = myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Ride> list = new LinkedList<Ride>();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    Ride ride = snap.getValue(Ride.class);
                    list.add(ride);
                }
                callback.onComplete(list);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onCancel();
            }
        });
    }


    public void getAllRidesAndObserve(double lastUpdateDate,
                                         final GetAllRidesAndObserveCallback callback) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("rides");

        myRef.orderByChild("lastUpdateDate").startAt(lastUpdateDate)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Ride> list = new LinkedList<Ride>();
                        for (DataSnapshot snap : dataSnapshot.getChildren()) {
                            Ride ride = snap.getValue(Ride.class);
                            list.add(ride);
                        }
                        callback.onComplete(list);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void getAllUsersAndObserve(double lastUpdateDate,
                                      final GetUsersAndObserveCallback callback) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("users");

        myRef.orderByChild("lastUpdateDate").startAt(lastUpdateDate)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<User> list = new LinkedList<>();
                        for (DataSnapshot snap : dataSnapshot.getChildren()) {
                            User user = snap.getValue(User.class);
                            list.add(user);
                        }
                        callback.onComplete(list);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void saveImage(Bitmap imageBmp, String name, final Model.SaveImageListener listener){
        FirebaseStorage storage = FirebaseStorage.getInstance();

        StorageReference imagesRef = storage.getReference().child("images").child(name);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = imagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                listener.fail();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                listener.complete(downloadUrl.toString());
            }
        });
    }


    public void getImage(String url, final Model.GetImageListener listener){
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference httpsReference = storage.getReferenceFromUrl(url);
        final long ONE_MEGABYTE = 1024 * 1024;
        httpsReference.getBytes(3* ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap image = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                listener.onSuccess(image);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                Log.d("TAG",exception.getMessage());
                listener.onFail();
            }
        });
    }



    interface GetRideCallback {
        void onComplete(Ride ride);

        void onCancel();
    }

    interface RideChangesListener {
        void onRideAdded(Ride ride);
        void onRideUpdated(Ride ride);
        void onRideDeleted(String rideId);
    }

    interface UserChangesListener {
        void onUserAdded(User user);
        void onUserDeleted(String userId);
    }

    interface GetAllRidesAndObserveCallback {
        void onComplete(List<Ride> list);
        void onCancel();
    }


    interface GetUsersAndObserveCallback {
        void onComplete(List<User> list);
        void onCancel();
    }
}

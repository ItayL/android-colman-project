package com.android.shaniitay.exrc5;

import android.app.Application;
import android.content.Context;

/**
 * Created by Shani on 15/06/2017.
 */

public class MyApplication extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }


    public static Context getMyContext(){
        return context;
    }
}
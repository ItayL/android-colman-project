package com.android.shaniitay.exrc5;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.shaniitay.exrc5.CustomViews.CustomDatePicker;
import com.android.shaniitay.exrc5.CustomViews.CustomTimePicker;
import com.android.shaniitay.exrc5.Filter.FilterConditions;

import java.sql.Time;
import java.util.Calendar;

import static java.lang.Long.getLong;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFilterInteraction} interface
 * to handle interaction events.
 * Use the {@link FilterFragment#newFilterFragment} factory method to
 * create an instance of this fragment.
 */
public class FilterFragment extends Fragment {
    private OnFilterInteraction mListener;
    private Calendar _selectedDateCalendar;
    private static String LAST_SELECTED_CALENDAR = "LAST_SELECTED_CALENDAR";

    public FilterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FilterFragment.
     */
    public static FilterFragment newFilterFragment() {
        FilterFragment fragment = new FilterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter, container, false);

        _selectedDateCalendar = Calendar.getInstance();
        if (savedInstanceState != null) {
            long selectedTime = savedInstanceState.getLong(LAST_SELECTED_CALENDAR);
            if (selectedTime != 0) {
                _selectedDateCalendar.setTime(new Time(selectedTime));
            }
        }

        assignButtonListener(view);
        getActivity().getActionBar().setTitle("Filter Rides");

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putLong(LAST_SELECTED_CALENDAR, _selectedDateCalendar.getTime().getTime());
        super.onSaveInstanceState(outState);
    }

    private void assignButtonListener(final View view) {
        Button cancelButton = (Button) view.findViewById(R.id.filter_cancle_btn);
        Button filterButton = (Button) view.findViewById(R.id.filter_filter_btn);
        final EditText txtSource = (EditText) view.findViewById(R.id.filter_txt_source);
        final EditText txtDestination = (EditText) view.findViewById(R.id.filter_txt_destination);
        final CustomTimePicker timePicker = (CustomTimePicker) view.findViewById(R.id.filter_time_picker);
        timePicker.setSelectedTime(_selectedDateCalendar);
        final CustomDatePicker datePicker = (CustomDatePicker) view.findViewById(R.id.filter_date_picker);
        datePicker.setSelectedDate(_selectedDateCalendar);

        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FilterConditions filterConditions = new FilterConditions();
                filterConditions.destination = txtDestination.getText().toString();
                filterConditions.source = txtSource.getText().toString();
                filterConditions.date = _selectedDateCalendar;

                mListener.onFilterPressed(filterConditions);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFilterCanceled();
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFilterInteraction) {
            mListener = (OnFilterInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFilterInteraction");
        }
    }


    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFilterInteraction) {
            mListener = (OnFilterInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFilterInteraction");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFilterInteraction {
        void onFilterPressed(FilterConditions filterConditions);

        void onFilterCanceled();
    }
}

package com.android.shaniitay.exrc5;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.shaniitay.exrc5.Filter.FilterConditions;
import com.android.shaniitay.exrc5.Model.Model;
import com.android.shaniitay.exrc5.Model.Ride;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RidesListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RidesListFragment#newRidestListFragment} factory method to
 * create an instance of this fragment.
 */
public class RidesListFragment extends Fragment implements Model.ModelUpdatesListener {

    private final static String ARG_FILTER_CONDITIONS = "filterConditions";
    private static final String ARG_FRAGMENT_TITLE = "fragmentTitleArg";
    private OnFragmentInteractionListener mListener;
    final RidesAdapter adapter = new RidesAdapter();
    List<Ride> rideList = new ArrayList<>();
    private View _bigMidLoadingAnimationView;
    private View _smallUpdateLoadingAnimationView;
    private Menu _actionMenu;
    private View _rootView;
    private FilterConditions _filterConditions;
    private String _fragmentTitle;
    private Button _addBtn;

    public RidesListFragment() {
        // Required empty public constructor
        Model.instance().addListener("RidesListFragment", this);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RidesListFragment.
     */
    public static RidesListFragment newRidestListFragment(FilterConditions filterConditions, String fragmentTitle) {
        RidesListFragment fragment = new RidesListFragment();
        Bundle args = new Bundle();
        if (filterConditions != null) {
            args.putSerializable(ARG_FILTER_CONDITIONS, filterConditions);
        }
        args.putString(ARG_FRAGMENT_TITLE, fragmentTitle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(ARG_FILTER_CONDITIONS)) {
                _filterConditions = (FilterConditions) getArguments().getSerializable(ARG_FILTER_CONDITIONS);
            }
            _fragmentTitle = getArguments().getString(ARG_FRAGMENT_TITLE);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        _rootView = inflater.inflate(R.layout.fragment_rides_list, container, false);

        getActivity().getActionBar().setTitle(_fragmentTitle);
        _addBtn = (Button) _rootView.findViewById(R.id.rides_list_add_ride_btn);
        _addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onNewRidePressed();
            }
        });
        return _rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (_filterConditions == null) {
            inflater.inflate(R.menu.rides_list_menu, menu);
        }
        _actionMenu = menu;

        assignAnimationViews();

        uiEnterLoadingMode();

        ListView list = initRidesListUiAsync();
        assignClickListeners(list);
    }

    private void assignAnimationViews() {
        _bigMidLoadingAnimationView = _rootView.findViewById(R.id.big_mid_loading_animation);
        _smallUpdateLoadingAnimationView = _rootView.findViewById(R.id.small_update_loading_animation);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.btnMyRides:

                mListener.onMyRidesPressed();
                break;
            case R.id.btnFilterRides:
                mListener.onFilterPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private ListView initRidesListUiAsync() {
        final RidesListFragment thisFregment = this;
        Model.instance().getAllRidesAndObserve(_filterConditions, new Model.GetAllRidesAndObserveCallback() {
            @Override
            public void onComplete(List<Ride> list) {
                uiExitLoadingMode();
                rideList = list;
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancel() {
                ToastsUtils.raiseToast(thisFregment.getActivity(), "An Error accrued, please try reloading the app");
            }
        });
        ListView list = (ListView) _rootView.findViewById(R.id.ridesListListView);

        list.setAdapter(adapter);
        return list;
    }

    private void uiEnterLoadingMode() {
        setActionMenuEnabled(false);
        _addBtn.setEnabled(false);
        addLoadingAnimation();
    }

    private void uiExitLoadingMode() {
        setActionMenuEnabled(true);
        _addBtn.setEnabled(true);
        removeLoadingAnimation();
    }

    private void setActionMenuEnabled(boolean enabled) {

        for (int i = 0; _actionMenu.size() > i; i++) {
            _actionMenu.getItem(i).setEnabled(enabled);
        }
    }

    private void addLoadingAnimation() {
        if (rideList == null || rideList.isEmpty()) {
            _bigMidLoadingAnimationView.setVisibility(View.VISIBLE);
        } else {
            _smallUpdateLoadingAnimationView.setVisibility(View.VISIBLE);
        }
    }

    private void removeLoadingAnimation() {
        _bigMidLoadingAnimationView.setVisibility(View.GONE);
        _smallUpdateLoadingAnimationView.setVisibility(View.GONE);
    }

    private void assignClickListeners(ListView list) {
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mListener.onRideSelected(rideList.get(i).getId());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFilterInteraction");
        }
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFilterInteraction");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onModelUpdated() {
        final RidesListFragment thisFregment = this;
        Model.instance().getAllRidesAndObserve(_filterConditions, new Model.GetAllRidesAndObserveCallback() {
            @Override
            public void onComplete(List<Ride> list) {
                rideList = list;
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancel() {
                ToastsUtils.raiseToast(thisFregment.getActivity(), "An Error accrued, the list is not synced with the server");
            }
        });
    }

    @Override
    public void onRideUpdated(Ride updatedRide) {

        for (int i = 0; i < rideList.size(); i++) {
            if (rideList.get(i).id.equals(updatedRide.getId())) {
                rideList.remove(i);
                rideList.add(updatedRide);
                break;
            }
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onRideCreated(Ride createdRide) {
        if (_filterConditions == null) {
            rideList.add(createdRide);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onRideDeleted(String rideId) {
        for (int i = 0; i < rideList.size(); i++) {
            if (rideList.get(i).id.equals(rideId)) {
                rideList.remove(i);
                break;
            }
        }

        adapter.notifyDataSetChanged();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onRideSelected(String rideId);

        void onNewRidePressed();

        void onFilterPressed();

        void onMyRidesPressed();
    }

    private class RidesAdapter extends BaseAdapter {

        @Override
        public void notifyDataSetChanged() {
            Collections.sort(rideList, new Comparator<Ride>() {
                @Override
                public int compare(Ride ride1, Ride ride2) {
                    return ride1.getRideDate().compareTo(ride2.getRideDate());
                }
            });
            super.notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return rideList.size();
        }

        @Override
        public Object getItem(int i) {
            return rideList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = inflateNewRow();
            }

            Ride rowRide = rideList.get(i);
            bindRowToRide(i, view, rowRide);

            return view;
        }

        private void bindRowToRide(int i, View view, Ride rowRide) {
            TextView sourceTv = (TextView) view.findViewById(R.id.rideRowSource);
            TextView destTv = (TextView) view.findViewById(R.id.rideRowDestination);
            TextView timeTv = (TextView) view.findViewById(R.id.rideRowTime);
            sourceTv.setText(rowRide.getSource());
            destTv.setText(rowRide.getDestination());
            String rideDateString = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.ENGLISH).format(rowRide.getRideDate());
            timeTv.setText(rideDateString);
        }

        private View inflateNewRow() {
            View view;
            view = getActivity().getLayoutInflater().inflate(R.layout.rides_list_row, null);

            return view;
        }
    }
}

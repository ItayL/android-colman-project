package com.android.shaniitay.exrc5.Model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.android.shaniitay.exrc5.Filter.FilterConditions;
import com.android.shaniitay.exrc5.MyApplication;

import java.sql.Time;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Shani on 15/06/2017.
 */

public class RideSql implements RideModelInterface {
    final public static RideSql Instance = new RideSql();
    private SqLightDB _dbInstance;
    private static final String
            RIDE_TABLE = "rides_table",
            ID_COL = "id",
            SOURCE_COL = "source",
            DESTINATION_COL = "destination",
            PHONE_COL = "phone",
            IMAGE_URL_COL = "imageUrl",
            DATE_COL = "date",
            PLACES_NUM_COL = "places_num",
            CREATOR_COL = "creator";

    public RideSql() {
        this._dbInstance = new SqLightDB(MyApplication.getMyContext());
    }

    @Override
    public List<Ride> getAllRides(FilterConditions filterConditions) {
        try (SQLiteDatabase dbRead = _dbInstance.getReadableDatabase()) {
            String whereStr = getWhereCondition(filterConditions);
            Cursor cursor = dbRead.query(RIDE_TABLE, null, whereStr, null, null, null, null);
            List<Ride> retList = readCursorToRidesList(cursor);

            return retList;
        }
    }

    private String getWhereCondition(FilterConditions filterConditions) {
        if (filterConditions == null) {
            return null;
        }

        String whereStr = "1 = 1 ";

        if (filterConditions.source != null && !filterConditions.source.isEmpty()) {
            whereStr += " AND " + SOURCE_COL + " LIKE('%" + filterConditions.source + "%') ";
        }
        if (filterConditions.destination != null && !filterConditions.destination.isEmpty()) {
            whereStr += " AND " + DESTINATION_COL + " LIKE('%" + filterConditions.destination + "%') ";
        }
        if (filterConditions.date != null) {
            whereStr +=  " AND " + DATE_COL + " > " + filterConditions.date.getTime().getTime();
        }
        if (filterConditions.driver != null && !filterConditions.driver.isEmpty()) {
            whereStr +=  " AND " + CREATOR_COL + " = '" + filterConditions.driver + "'";
        }

        return whereStr;
    }

    @NonNull
    private List<Ride> readCursorToRidesList(Cursor cursor) {
        List<Ride> retList = new LinkedList<>();

        while (cursor.moveToNext()) {
            ContentValues rowCV = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(cursor, rowCV);
            Ride ride = CVToRide(rowCV);
            retList.add(ride);
        }
        return retList;
    }

    @Override
    public Ride getRide(String id) {
        try (SQLiteDatabase dbRead = _dbInstance.getReadableDatabase()) {
            String whereStr = ID_COL + "='" + id + "'";
            Cursor cursor = dbRead.query(RIDE_TABLE, null, whereStr, null, null, null, null);
            List<Ride> retList = readCursorToRidesList(cursor);

            if (retList.isEmpty())
                return null;
            else
                return retList.get(0);
        }
    }

    // Add the ride
    @Override
    public void addRide(Ride ride) {
        try (SQLiteDatabase dbWrite = _dbInstance.getWritableDatabase()) {
            ContentValues cv = rideToCV(ride);
            dbWrite.replace(RIDE_TABLE, ID_COL, cv);

            savePassengers(ride);
        }
    }

    private void savePassengers(Ride ride) {
        if (ride.passengers == null)
            return;

        for (User passenger : ride.passengers.values()) {
            UserSql.Instance.addPassenger(passenger.id, ride.id);
        }
    }

    @Override
    public void addPassengerToRide(String rideId, User user) {
        UserSql.Instance.addPassenger(user.id, rideId);
    }

    // Update the student details
    @Override
    public void updateRide(String id,
                           String name,
                           String address,
                           String phone,
                           Date rideDate,
                           Map<String, User> passengers,
                           int numberOfPlaces,
                           String imageUrl, User creator) {

        String whereStr = ID_COL + "='" + id + "'";
        Ride newRide = new Ride(name, address, id, phone, rideDate, numberOfPlaces, imageUrl, creator);
        ContentValues cv = rideToCV(newRide);

        try (SQLiteDatabase dbWrite = _dbInstance.getWritableDatabase()) {
            dbWrite.update(RIDE_TABLE, cv, whereStr, null);
            if (passengers != null) {
                newRide.passengers = passengers;
                savePassengers(newRide);
            }
        }
    }

    // Delete the ride
    @Override
    public void delete(Ride ride) {
        try (SQLiteDatabase dbWrite = _dbInstance.getWritableDatabase()) {
            String whereStr = ID_COL + "='" + ride.getId() + "'";
            dbWrite.delete(RIDE_TABLE, whereStr, null);
        }
    }

    public void delete(String rideId) {
        try (SQLiteDatabase dbWrite = _dbInstance.getWritableDatabase()) {
            String whereStr = ID_COL + "='" + rideId + "'";
            dbWrite.delete(RIDE_TABLE, whereStr, null);
        }
    }

    public boolean checkRideIdIsTaken(String id) {
        Ride ride = getRide(id);

        if (ride == null)
            return false;
        else
            return true;
    }

    public void createRidesTable(SQLiteDatabase db) {
        String ridesTableCreateCommand =
                "create table " + RIDE_TABLE + " (" +
                        ID_COL + " TEXT PRIMARY KEY, " +
                        SOURCE_COL + " TEXT, " +
                        DESTINATION_COL + " TEXT, " +
                        PHONE_COL + " TEXT, " +
                        IMAGE_URL_COL + " TEXT, " +
                        DATE_COL + " NUMBER, " +
                        PLACES_NUM_COL + " NUMBER, " +
                        CREATOR_COL + " TEXT);";

        db.execSQL(ridesTableCreateCommand);
    }

    public void resetRidesTable(SQLiteDatabase db) {
        try {
            db.execSQL("drop table " + RIDE_TABLE + ";");
        } catch (Exception ex) {

        }
        createRidesTable(db);
    }

    private static ContentValues rideToCV(Ride ride) {
        ContentValues retCV = new ContentValues();
        retCV.put(ID_COL, ride.getId());
        retCV.put(SOURCE_COL, ride.getSource());
        retCV.put(DESTINATION_COL, ride.getDestination());
        retCV.put(PHONE_COL, ride.getPhone());
        retCV.put(IMAGE_URL_COL, ride.getImageUrl());
        retCV.put(DATE_COL, ride.getRideDate().getTime());
        retCV.put(PLACES_NUM_COL, ride.getNumberOfPlaces());
        retCV.put(CREATOR_COL, ride.getCreator().id);

        return retCV;
    }

    private static Ride CVToRide(ContentValues cv) {
        Ride retRide = new Ride(
                cv.getAsString(SOURCE_COL),
                cv.getAsString(DESTINATION_COL),
                cv.getAsString(ID_COL),
                cv.getAsString(PHONE_COL),
                new Date(Long.parseLong(cv.getAsString(DATE_COL))),
                cv.getAsInteger(PLACES_NUM_COL),
                cv.getAsString(IMAGE_URL_COL),
                getUserById(cv.getAsString(CREATOR_COL)));
        Map<String, User> passengers = UserSql.Instance.getPassengers(retRide.id);
        retRide.setPassengers(passengers);

        return retRide;
    }

    private static User getUserById(String userString) {
        return UserSql.Instance.getUser(userString);
    }

}

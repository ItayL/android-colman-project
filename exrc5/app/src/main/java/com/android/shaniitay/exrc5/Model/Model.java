package com.android.shaniitay.exrc5.Model;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.URLUtil;

import com.android.shaniitay.exrc5.Filter.FilterConditions;
import com.android.shaniitay.exrc5.MyApplication;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.android.shaniitay.exrc5.Model.ModelFiles.saveImageToFile;

public class Model {
    final private static Model _instance = new Model();
    private ModelFirebase modelFirebase;
    private Map<String, ModelUpdatesListener> _listeners = new HashMap<>();
    public static final String RIDES_LAST_UPDATE_DATE_PREF_KEY = "RidesLastUpdateDate";

    private Model() {
        modelFirebase = new ModelFirebase();
        addRidesListener();
    }

    public static Model instance() {
        return _instance;
    }

    public void addListener(ModelUpdatesListener modelUpdatesListener) {
        addListener(UUID.randomUUID().toString(), modelUpdatesListener);
    }

    public void addListener(String name, ModelUpdatesListener modelUpdatesListener) {
        _listeners.put(name, modelUpdatesListener);
    }

    public void addRidesListener() {
        modelFirebase.listenToRidesUpdates(new ModelFirebase.RideChangesListener() {
            @Override
            public void onRideAdded(Ride ride) {
                RideSql.Instance.addRide(ride);
                updateLastUpdateTime((float) ride.lastUpdateDate);
                notifyListenersRideCreated(ride);
            }

            @Override
            public void onRideUpdated(Ride ride) {
                RideSql.Instance.addRide(ride);
                updateLastUpdateTime((float) ride.lastUpdateDate);
                notifyListenersRideUpdated(ride);
            }

            @Override
            public void onRideDeleted(String rideId) {
                RideSql.Instance.delete(rideId);
                notifyListenersRideDeleted(rideId);
            }
        });
        modelFirebase.listenToUserUpdates(new ModelFirebase.UserChangesListener() {

            @Override
            public void onUserAdded(User user) {
                UserSql.Instance.addUser(user);
            }

            @Override
            public void onUserDeleted(String userId) {
            }
        });
    }

    private void notifyListenersModelChanged() {
        for (Model.ModelUpdatesListener listener : _listeners.values()) {
            listener.onModelUpdated();
        }
    }

    private void notifyListenersRideUpdated(Ride updatedRide) {
        for (Model.ModelUpdatesListener listener : _listeners.values()) {
            listener.onRideUpdated(updatedRide);
        }
    }

    private void notifyListenersRideDeleted(String rideId) {
        for (Model.ModelUpdatesListener listener : _listeners.values()) {
            listener.onRideDeleted(rideId);
        }
    }

    private void notifyListenersRideCreated(Ride createdRide) {
        for (Model.ModelUpdatesListener listener : _listeners.values()) {
            listener.onRideCreated(createdRide);
        }
    }

    public void getAllRidesAndObserve(final FilterConditions filterConditions, final GetAllRidesAndObserveCallback callback) {
        //1. get local lastUpdateTade
        SharedPreferences pref = MyApplication.getMyContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        final double lastUpdateDate = pref.getFloat(RIDES_LAST_UPDATE_DATE_PREF_KEY, 0);
        Log.d("TAG", "lastUpdateDate: " + lastUpdateDate);

        //2. get updated records from FB
        modelFirebase.getAllUsersAndObserve(lastUpdateDate, new ModelFirebase.GetUsersAndObserveCallback() {
            @Override
            public void onComplete(List<User> list) {
                for (User user : list) {
                    UserSql.Instance.addUser(user);
                }

                modelFirebase.getAllRidesAndObserve(lastUpdateDate, new ModelFirebase.GetAllRidesAndObserveCallback() {
                    @Override
                    public void onComplete(List<Ride> list) {
                        double newLastUpdateDate = lastUpdateDate;
                        for (Ride ride : list) {
                            //3. update the local db
                            try {
                                RideSql.Instance.addRide(ride);
                                //4. update the lastUpdateTade
                                if (newLastUpdateDate < ride.lastUpdateDate) {
                                    newLastUpdateDate = ride.lastUpdateDate;
                                }
                            } catch (Exception ex) {
                                int a = ex.hashCode();
                            }
                        }
                        updateLastUpdateTime((float) newLastUpdateDate);
                        Log.d("TAG", RIDES_LAST_UPDATE_DATE_PREF_KEY + newLastUpdateDate);


                        //5. read from local db
                        List<Ride> data = RideSql.Instance.getAllRides(filterConditions);

                        //6. return list of rides
                        callback.onComplete(data);
                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }

            @Override
            public void onCancel() {

            }
        });
    }

    private void updateLastUpdateTime(float newLastUpdateDate) {
        SharedPreferences.Editor prefEd = MyApplication.getMyContext().getSharedPreferences("TAG",
                Context.MODE_PRIVATE).edit();
        prefEd.putFloat(RIDES_LAST_UPDATE_DATE_PREF_KEY, newLastUpdateDate);
        prefEd.commit();
    }

    public Ride getRide(String id) {
        return RideSql.Instance.getRide(id);
    }

    public void addRide(final Ride ride, final AddRideListener listener) {
        Task<Void> rideCreationTask = modelFirebase.addRide(ride);

        rideCreationTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                listener.fail(ride.getId());
            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                listener.complete(ride.getId());
            }
        });
    }

    public void addPassengerToRide(final String rideId, final User user, final AddPassengersToRideListener listener) {
        Task<Void> passengersAddTask = modelFirebase.addPassengerToRide(rideId, user);
        passengersAddTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                listener.fail(rideId);
            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Ride ride = Model.instance().getRide(rideId);
                RideSql.Instance.addPassengerToRide(rideId, user);
                notifyListenersRideUpdated(ride);

                listener.complete(rideId);
            }
        });
    }

    public void updateRide(final String id,
                           String name,
                           String address,
                           String phone,
                           Date rideDate,
                           Map<String, User> passengers,
                           int numberOfPlaces,
                           String imageUrl,
                           User creator,
                           final UpdateRideListener listener) {

        Task<Void> updateRideTask =
            modelFirebase.updateRide(id, name, address, phone, rideDate, passengers, numberOfPlaces, imageUrl, creator);

        updateRideTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                listener.fail(id);
            }
        });
        updateRideTask.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                    listener.complete(id);
            }
        });
    }

    public void delete(final Ride ride, final DeleteRideListener listener) {
        Task<Void> rideDeleteTask = modelFirebase.delete(ride);

        rideDeleteTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                listener.fail(ride.getId());
            }
        });
        rideDeleteTask.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                listener.complete(ride.getId());
            }
        });
    }

    public void saveImage(final Bitmap imageBmp, final String name, final SaveImageListener listener) {
        modelFirebase.saveImage(imageBmp, name, new SaveImageListener() {
            @Override
            public void complete(String url) {
                String fileName = URLUtil.guessFileName(url, null, null);
                saveImageToFile(imageBmp, fileName);
                listener.complete(url);
            }

            @Override
            public void fail() {
                listener.fail();
            }
        });
    }

    public void getImage(final String url, final GetImageListener listener) {
        //check if image exist locally
        final String fileName = URLUtil.guessFileName(url, null, null);
        ModelFiles.loadImageFromFileAsync(fileName, new ModelFiles.LoadImageFromFileAsync() {
            @Override
            public void onComplete(Bitmap bitmap) {
                if (bitmap != null) {
                    Log.d("TAG", "getImage from local success " + fileName);
                    listener.onSuccess(bitmap);
                } else {
                    modelFirebase.getImage(url, new GetImageListener() {
                        @Override
                        public void onSuccess(Bitmap image) {
                            String fileName = URLUtil.guessFileName(url, null, null);
                            saveImageToFile(image, fileName);
                            listener.onSuccess(image);
                        }

                        @Override
                        public void onFail() {
                            listener.onFail();
                        }
                    });

                }
            }
        });

    }

    public void addUser(final User user, final AddUserListener listener) {
        Task<Void> addUserTask = modelFirebase.addUser(user);

        addUserTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                listener.fail(user.getId());
            }
        });
        addUserTask.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                UserSql.Instance.addUser(user);
                listener.complete(user.getId());
            }
        });
    }

    public interface ModelUpdatesListener {
        void onModelUpdated();

        void onRideUpdated(Ride updatedRide);

        void onRideCreated(Ride createdRide);

        void onRideDeleted(String rideId);
    }

    public interface SaveImageListener {
        void complete(String url);

        void fail();
    }

    public interface AddRideListener {
        void complete(String newRideId);

        void fail(String newRideId);
    }

    public interface UpdateRideListener {
        void complete(String updatedRideId);

        void fail(String updatedRideId);
    }

    public interface DeleteRideListener {
        void complete(String deletedRideId);

        void fail(String deletedRideId);
    }

    public interface AddPassengersToRideListener {
        void complete(String rideId);

        void fail(String rideId);
    }

    public interface GetImageListener {
        void onSuccess(Bitmap image);

        void onFail();
    }

    public interface GetAllRidesAndObserveCallback {
        void onComplete(List<Ride> list);

        void onCancel();
    }

    public interface AddUserListener {
        void complete(String userId);

        void fail(String userId);
    }
}
package com.android.shaniitay.exrc5.CustomViews;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.TimePicker;

import java.sql.Time;
import java.util.Calendar;

public class CustomTimePicker extends EditText implements CustomOnTimeSetListener {
    private Calendar _selectedTimeCalender = Calendar.getInstance();

    public CustomTimePicker(Context context) {
        super(context);
        setInputType(0);
        rebindUiToSelectedValues();
    }

    public CustomTimePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        setInputType(0);
        rebindUiToSelectedValues();
    }

    public CustomTimePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setInputType(0);
        rebindUiToSelectedValues();
    }

    public void setSelectedTime(Calendar timeCalendar) {
        _selectedTimeCalender = timeCalendar;
        rebindUiToSelectedValues();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        if (event.getAction() == MotionEvent.ACTION_DOWN){
            CustomTimePickerDialog tpd =
                CustomTimePickerDialog.newInstance(getId(), _selectedTimeCalender);
            tpd.show(((Activity)getContext()).getFragmentManager(),"TAG");
            return true;
        }
        return true;
    }

    @Override
    public void onTimeSet(int hour, int min) {
        _selectedTimeCalender.set(Calendar.HOUR_OF_DAY, hour);
        _selectedTimeCalender.set(Calendar.MINUTE, min);
        rebindUiToSelectedValues();
    }

    private void rebindUiToSelectedValues() {
        String displayTime = DateTimeDisplayUtils.getDisplayTime(
                _selectedTimeCalender.get(Calendar.HOUR_OF_DAY),
                _selectedTimeCalender.get(Calendar.MINUTE)
        );
        setText(displayTime);
        setTextSize(16);
    }

    public static class CustomTimePickerDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        private static final String TAG_ARG_KEY = "TAG_ARG_KEY";
        private static final String HOUR_ARG_KEY = "HOUR_ARG_KEY";
        private static final String MINUTE_ARG_KEY = "MINUTE_ARG_KEY";
        CustomOnTimeSetListener listener;

        public static CustomTimePickerDialog newInstance(int tag, Calendar timeCalendar) {
            CustomTimePickerDialog timePickerDialog = new CustomTimePickerDialog();
            Bundle args = new Bundle();
            args.putInt(TAG_ARG_KEY, tag);
            args.putInt(HOUR_ARG_KEY, timeCalendar.get(Calendar.HOUR_OF_DAY));
            args.putInt(MINUTE_ARG_KEY, timeCalendar.get(Calendar.MINUTE));
            timePickerDialog.setArguments(args);
            return timePickerDialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            super.onCreateDialog(savedInstanceState);

            int hour = getArguments().getInt(HOUR_ARG_KEY);
            int minute = getArguments().getInt(MINUTE_ARG_KEY);
            Dialog timePicker = new TimePickerDialog(getActivity(), this, hour, minute, false);

            if (getArguments() != null) {
                int tag = getArguments().getInt(TAG_ARG_KEY);
                listener = (CustomOnTimeSetListener) getActivity().findViewById(tag);
            }

            return timePicker;
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            if (listener != null) {
                listener.onTimeSet(hourOfDay, minute);
            }
        }
    }
}




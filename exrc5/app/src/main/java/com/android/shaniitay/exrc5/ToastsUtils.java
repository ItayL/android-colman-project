package com.android.shaniitay.exrc5;

import android.app.Activity;
import android.graphics.Color;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by itay on 5/15/2017.
 */

public class ToastsUtils {

    public static void raiseToast(Activity callingActivity, String message) {
        if (callingActivity != null) {
            Toast toast = Toast.makeText(callingActivity.getApplicationContext(),
                    message, Toast.LENGTH_SHORT);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(Color.RED);
            toast.show();
        }
    }
}

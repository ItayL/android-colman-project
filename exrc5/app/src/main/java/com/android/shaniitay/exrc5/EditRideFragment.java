package com.android.shaniitay.exrc5;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.MediaStore;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.shaniitay.exrc5.CustomViews.CustomDatePicker;
import com.android.shaniitay.exrc5.CustomViews.CustomTimePicker;
import com.android.shaniitay.exrc5.Model.InputFieldsValidatior;
import com.android.shaniitay.exrc5.Model.Model;
import com.android.shaniitay.exrc5.Model.Ride;
import com.android.shaniitay.exrc5.Model.User;

import java.io.ByteArrayOutputStream;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditRideFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditRideFragment#newEditRideFragment} factory method to
 * create an instance of this fragment.
 */
public class EditRideFragment extends Fragment {
    private static final String ARG_RIDE_ID = "rideId";
    private static final String ARG_IMAGE_CHANGED = "imageChanged";
    private static final String ARG_BITMAP = "bitmap";
    private static String LAST_SELECTED_CALENDAR = "LAST_SELECTED_CALENDAR";

    private String _rideId = "";

    private OnFragmentInteractionListener mListener;
    private ImageView imageView;
    Bitmap imageBitmap;
    private boolean imageChanged = false;
    private Calendar _selectedDateCalendar;
    private View _saveBtn;

    public EditRideFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param rideId .
     * @return A new instance of fragment EditRideFragment.
     */
    public static EditRideFragment newEditRideFragment(String rideId) {
        EditRideFragment fragment = new EditRideFragment();
        Bundle args = new Bundle();
        args.putString(ARG_RIDE_ID, rideId);
        args.putBoolean(ARG_IMAGE_CHANGED, false);

        fragment.setArguments(args);
        return fragment;
    }

    private void bindUiToRide(final Ride editedRide, View view) {

        TextView sourceTv = (TextView) view.findViewById(R.id.editSource);
        sourceTv.setText(editedRide.getSource());

        TextView destinationTv = (TextView) view.findViewById(R.id.editDestination);
        destinationTv.setText(editedRide.getDestination());

        TextView phoneTv = (TextView) view.findViewById(R.id.editPhone);
        phoneTv.setText(editedRide.getPhone());

        TextView places = (TextView) view.findViewById(R.id.edit_ride_places_number);
        places.setText(String.valueOf(editedRide.getNumberOfPlaces()));

        CustomTimePicker timePck = (CustomTimePicker) view.findViewById(R.id.edit_time_picker);
        timePck.setSelectedTime(_selectedDateCalendar);

        CustomDatePicker datePck = (CustomDatePicker) view.findViewById(R.id.edit_date_picker);
        datePck.setSelectedDate(_selectedDateCalendar);

        imageView = (ImageView) view.findViewById(R.id.edit_ride_image_view);
        imageView.setTag(editedRide.imageUrl);
        final View progressBar = view.findViewById(R.id.edit_ride_image_progress_bar);

        if (editedRide.imageUrl != null && !editedRide.imageUrl.isEmpty() && !editedRide.imageUrl.equals("") && imageBitmap == null) {
            progressBar.setVisibility(View.VISIBLE);
            Model.instance().getImage(editedRide.imageUrl, new Model.GetImageListener() {
                @Override
                public void onSuccess(Bitmap image) {
                    String tagUrl = imageView.getTag().toString();
                    if (tagUrl.equals(editedRide.imageUrl) && imageBitmap == null) {
                        imageView.setImageBitmap(image);
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFail() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    }


    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
        }
    }


    private void assignBtnListeners(final Ride editedRide, View view) {
        final View viewFinal = view;
        final Button saveBtn = (Button) view.findViewById(R.id.btnSaveEdit);
        Button cancelBtn = (Button) view.findViewById(R.id.btnCancelEdit);
        Button deleteBtn = (Button) view.findViewById(R.id.btnDeleteEdit);


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                saveBtn.setEnabled(false);
                final EditText sourceTv = (EditText) viewFinal.findViewById(R.id.editSource);
                final EditText destinationTv = (EditText) viewFinal.findViewById(R.id.editDestination);
                final EditText phoneTv = (EditText) viewFinal.findViewById(R.id.editPhone);
                EditText placesTv = (EditText) viewFinal.findViewById(R.id.edit_ride_places_number);

                if (!InputFieldsValidatior.validateFields(placesTv, sourceTv, destinationTv, phoneTv, getActivity()))
                    return;

                final CustomTimePicker timePicker = (CustomTimePicker) viewFinal.findViewById(R.id.edit_time_picker);
                final CustomDatePicker datePicker = (CustomDatePicker) viewFinal.findViewById(R.id.edit_date_picker);

                final int places = Integer.valueOf(placesTv.getText().toString());

                final Ride currentRide = Model.instance().getRide(_rideId);

                if (places < currentRide.getPassengers().size()) {
                    ToastsUtils.raiseToast(getActivity(), "Passengers Already Joined The Ride, You Cant Decrease Available Places..:(");
                    saveBtn.setEnabled(true);
                    return;
                }

                if (imageBitmap != null && imageChanged) {
                    Model.instance().saveImage(imageBitmap, _rideId +
                            UUID.randomUUID().toString() + ".jpeg", new Model.SaveImageListener() {
                        @Override
                        public void complete(String url) {
                            currentRide.imageUrl = url;

                            // Update the student (the id can also edited)
                            updateRideInDB(_rideId, sourceTv.getText().toString(), destinationTv.getText().toString(),
                                    phoneTv.getText().toString(), _selectedDateCalendar.getTime(), currentRide.getPassengers(), places, url,
                                    currentRide.getCreator());
                        }

                        @Override
                        public void fail() {
                            //notify operation fail,...
                            saveBtn.setEnabled(true);
                            ToastsUtils.raiseToast(getActivity(), "Error Saving Image..");
                        }
                    });
                } else {
                    // Update the student (the id can also edited)
                    updateRideInDB(_rideId, sourceTv.getText().toString(), destinationTv.getText().toString(),
                            phoneTv.getText().toString(), _selectedDateCalendar.getTime(), currentRide.getPassengers(), places, currentRide.imageUrl,
                            currentRide.getCreator());
                }
            }

        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onEditCanceled(_rideId);
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Model.instance().delete(editedRide, new Model.DeleteRideListener() {
                    @Override
                    public void complete(String deletedRideId) {
                        mListener.onRideDeleted(_rideId);
                    }

                    @Override
                    public void fail(String deletedRideId) {
                        ToastsUtils.raiseToast(getActivity(), "Error Deleting Ride..");
                    }
                });
            }
        });


        imageView = (ImageView) view.findViewById(R.id.edit_ride_image_view);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
                imageChanged = true;
            }
        });
        if (imageBitmap != null) {
            imageView.setImageBitmap(imageBitmap);
        }
    }

    private void updateRideInDB(String id,
                                String name,
                                String address,
                                String phone,
                                Date rideDate,
                                Map<String, User> passengers,
                                int numberOfPlaces,
                                String imageUrl,
                                User creator) {
        Model.instance().updateRide(id, name, address, phone, rideDate, passengers, numberOfPlaces, imageUrl, creator,
                new Model.UpdateRideListener() {
                    @Override
                    public void complete(String updatedRideId) {
                        mListener.onRideSaved(_rideId);
                    }

                    @Override
                    public void fail(String updatedRideId) {
                        ToastsUtils.raiseToast(getActivity(), "Error Editing Ride..");
                        _saveBtn.setEnabled(true);
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mListener.onEditCanceled(_rideId);
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            _rideId = getArguments().getString(ARG_RIDE_ID);
            if (savedInstanceState != null) {
                if (savedInstanceState.containsKey(ARG_IMAGE_CHANGED)) {
                    imageChanged = savedInstanceState.getBoolean(ARG_IMAGE_CHANGED);
                }
                if (savedInstanceState.containsKey(ARG_BITMAP)) {
                    byte[] byteArray = savedInstanceState.getByteArray(ARG_BITMAP);
                    imageBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(ARG_IMAGE_CHANGED, imageChanged);

        if (imageBitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            outState.putByteArray(ARG_BITMAP, byteArray);
        }

        outState.putLong(LAST_SELECTED_CALENDAR, _selectedDateCalendar.getTime().getTime());
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_ride, container, false);

        final Ride editedRide = Model.instance().getRide(_rideId);
        _saveBtn = view.findViewById(R.id.btnSaveEdit);

        _selectedDateCalendar = Calendar.getInstance();
        boolean foundLastSavedTime = false;
        if (savedInstanceState != null) {
            long selectedTime = savedInstanceState.getLong(LAST_SELECTED_CALENDAR);
            if (selectedTime != 0) {
                _selectedDateCalendar.setTime(new Time(selectedTime));
                foundLastSavedTime = true;
            }
        }
        if (!foundLastSavedTime) {
            _selectedDateCalendar.setTime(editedRide.getRideDate());
        }

        bindUiToRide(editedRide, view);
        assignBtnListeners(editedRide, view);

        getActivity().getActionBar().setTitle("Edit Ride");

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFilterInteraction");
        }
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFilterInteraction");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onRideSaved(String id);

        void onRideDeleted(String id);

        void onEditCanceled(String id);
    }
}

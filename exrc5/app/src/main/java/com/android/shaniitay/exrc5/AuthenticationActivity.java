package com.android.shaniitay.exrc5;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.android.shaniitay.exrc5.Model.Model;
import com.android.shaniitay.exrc5.Model.User;
import com.facebook.CallbackManager;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

public class AuthenticationActivity extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener {

    public static User getCurrentLoginUser() {
        if (_currentLoginUser == null) {
            // Should not reach here
            throw new RuntimeException("No facebook user is logged in");
        }

        return _currentLoginUser;
    }

    private static User _currentLoginUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        _currentLoginUser = null;

        _profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                Profile oldProfile,
                Profile currentProfile) {

                if (currentProfile != null) {
                    _currentLoginUser = userFromFacebookProfile(currentProfile);
                    addUserToDbAndRedirectToApp();
                }
            }
        };

        Profile currentProfile = Profile.getCurrentProfile();
        if (currentProfile != null) {
            _currentLoginUser = userFromFacebookProfile(currentProfile);
            addUserToDbAndRedirectToApp();
        }
    }


    private void addUserToDbAndRedirectToApp() {
        final Activity thisActivity = this;

        Model.instance().addUser(_currentLoginUser, new Model.AddUserListener() {
            @Override
            public void complete(String userId) {
                redirectToMainActivity();
            }

            @Override
            public void fail(String userId) {
                ToastsUtils.raiseToast(thisActivity, "And Error Accrued");
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        throw new RuntimeException("Google connection error" + connectionResult);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        _facebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public static void logOutAndRedirectHere(Activity callingActivity) {

        LoginManager.getInstance().logOut();

        Intent authenticationActivityIntent = new Intent(callingActivity.getApplicationContext(), AuthenticationActivity.class);
        callingActivity.startActivity(authenticationActivityIntent);

        // Close calling activity and remove it from stack
        callingActivity.finish();
    }

    private void redirectToMainActivity() {
        Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(mainActivityIntent);

        // Remove current activity from stack
        finish();
    }

    @NonNull
    private static User userFromFacebookProfile(Profile currentUserProfile) {
        User user = new User(currentUserProfile.getId(), currentUserProfile.getName());
        return user;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        _profileTracker.stopTracking();
    }

    private static CallbackManager _facebookCallbackManager = CallbackManager.Factory.create();
    private ProfileTracker _profileTracker;
}

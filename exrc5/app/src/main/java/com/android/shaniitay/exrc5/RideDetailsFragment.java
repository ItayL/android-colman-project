package com.android.shaniitay.exrc5;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.shaniitay.exrc5.CustomViews.DateTimeDisplayUtils;
import com.android.shaniitay.exrc5.Model.Model;
import com.android.shaniitay.exrc5.Model.Ride;
import com.android.shaniitay.exrc5.Model.User;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RideDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RideDetailsFragment#newRideDetailsFragment} factory method to
 * create an instance of this fragment.
 */
public class RideDetailsFragment extends Fragment {
    private static final String ARG_RIDE_ID = "rideId";
    private String _rideId = "";

    private OnFragmentInteractionListener mListener;
    private View _rootView;
    private View _progressBar;

    public RideDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param rideId Parameter 1.
     * @return A new instance of fragment RideDetailsFragment.
     */
    public static RideDetailsFragment newRideDetailsFragment(String rideId) {
        RideDetailsFragment fragment = new RideDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_RIDE_ID, rideId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            _rideId = getArguments().getString(ARG_RIDE_ID);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_ride_details, container, false);
        _rootView = view;
        _progressBar = _rootView.findViewById(R.id.ride_details_image_progress_bar);

        final Ride ride = Model.instance().getRide(_rideId);
        bindRideToUi(ride);
        assignBtnListeners();

        Model.instance().addListener("RideDetailsFragment", new Model.ModelUpdatesListener() {
            @Override
            public void onModelUpdated() {

            }

            @Override
            public void onRideUpdated(Ride updatedRide) {
                if (_rideId.equalsIgnoreCase(updatedRide.getId())) {
                    bindRideToUi(updatedRide);
                }
            }

            @Override
            public void onRideCreated(Ride createdRide) {

            }

            @Override
            public void onRideDeleted(String rideId) {
                if (_rideId.equalsIgnoreCase(rideId)) {
                    ToastsUtils.raiseToast(getActivity(), "Viewed Ride Was Deleted By Its Creator");
                    ((Button) _rootView.findViewById(R.id.btnJoinRide)).setEnabled(false);
                }
            }
        });

        getActivity().getActionBar().setTitle("Ride Details");

        return view;
    }

    private void assignBtnListeners() {
        final Button joinRideBtn = (Button) _rootView.findViewById(R.id.btnJoinRide);
        final User currentUser = AuthenticationActivity.getCurrentLoginUser();
        final View progressBar = _progressBar;

        joinRideBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                Model.instance().addPassengerToRide(_rideId, currentUser, new Model.AddPassengersToRideListener() {
                    @Override
                    public void complete(String rideId) {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void fail(String rideId) {
                        ToastsUtils.raiseToast(getActivity(), "Error Adding Passengers To The Ride..");
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    private void chooseJoinRideButtonEnabled(Ride ride) {
        Button joinRideBtn = (Button) _rootView.findViewById(R.id.btnJoinRide);
        User currentUser = AuthenticationActivity.getCurrentLoginUser();

        int leftFreePlaces = ride.getNumberOfPlaces() - ride.getPassengers().size();
        boolean userIsRideCreator = currentUser.equals(ride.getCreator());
        if (leftFreePlaces > 0 && !userIsRideCreator && !userIsPassengerOnRide(currentUser, ride)) {
            joinRideBtn.setEnabled(true);
        } else {
            joinRideBtn.setEnabled(false);
        }
    }

    private static boolean userIsPassengerOnRide(User user, Ride ride) {
        for (User passenger : ride.getPassengers().values()) {
            if (passenger.equals(user)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Ride ride = Model.instance().getRide(_rideId);

        boolean currentUserCreatedDisplayedRide = ride.getCreator().equals(AuthenticationActivity.getCurrentLoginUser());
        if (currentUserCreatedDisplayedRide) {
            inflater.inflate(R.menu.ride_details_menu, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.btnEditRide:
                mListener.onUpdateRidePressed(_rideId);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFilterInteraction");
        }
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFilterInteraction");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void bindRideToUi(Ride ride) {
        TextView source = (TextView) _rootView.findViewById(R.id.ride_details_source);
        source.setText(ride.getSource());

        TextView phone = (TextView) _rootView.findViewById(R.id.ride_details_phone);
        phone.setText(ride.getPhone());

        TextView destination = (TextView) _rootView.findViewById(R.id.ride_details_destination);
        destination.setText(ride.getDestination());

        TextView passengers = (TextView) _rootView.findViewById(R.id.ride_passengers);
        passengers.setText(getPassengersString(ride));

        Calendar rideDateCal = Calendar.getInstance();
        rideDateCal.setTime(ride.getRideDate());
        String rideTimeText =
            DateTimeDisplayUtils.getDisplayTime(ride.getRideDate().getHours(), ride.getRideDate().getMinutes());
        String rideDateText = DateTimeDisplayUtils.getDisplayDate(rideDateCal);
        TextView birthTime = (TextView) _rootView.findViewById(R.id.ride_details_time);
        birthTime.setText(rideDateText + " " + rideTimeText);

        TextView driverText = (TextView) _rootView.findViewById(R.id.ride_details_driver);
        driverText.setText(ride.getCreator().getName());

        TextView places = (TextView) _rootView.findViewById(R.id.ride_details_places_number);
        int freePlaces = ride.getNumberOfPlaces() - ride.getPassengers().size();
        places.setText(String.valueOf(freePlaces));

        chooseJoinRideButtonEnabled(ride);

        final ImageView imageView = (ImageView) _rootView.findViewById(R.id.ride_details_image_view);
        final View progressBar = _progressBar;

        if (ride.imageUrl != null && !ride.imageUrl.isEmpty() && !ride.imageUrl.equals("")) {
            progressBar.setVisibility(View.VISIBLE);
            Model.instance().getImage(ride.imageUrl, new Model.GetImageListener() {
                @Override
                public void onSuccess(Bitmap image) {
                    imageView.setImageBitmap(image);
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFail() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    }

    private static String getPassengersString(Ride ride) {

        if (ride.getPassengers().size() == 0)
            return "";

        String passengersStr = "";
        for (User passenger : ride.getPassengers().values()) {
            passengersStr += passenger.getName() + ",";
        }

        passengersStr = passengersStr.substring(0, passengersStr.length() - 1);
        return passengersStr;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onUpdateRidePressed(String rideId);
    }
}

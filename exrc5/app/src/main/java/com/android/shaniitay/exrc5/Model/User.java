package com.android.shaniitay.exrc5.Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by itay235711 on 14/06/2017.
 */

public class User implements Serializable {
    public String name;
    public String id;

    public User() {};

    public User(String id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof User))
            return false;

        User otherUser = ((User) obj);
        return this.getId().equals(otherUser.getId()) && this.getName().equals(otherUser.getName());
    }

    public String toJSON(){

        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id", id);
            jsonObject.put("name", name);

            return jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }
}

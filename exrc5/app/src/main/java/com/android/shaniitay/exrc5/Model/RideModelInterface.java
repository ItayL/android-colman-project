package com.android.shaniitay.exrc5.Model;

import android.database.sqlite.SQLiteDatabase;

import com.android.shaniitay.exrc5.Filter.FilterConditions;

import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Shani on 15/06/2017.
 */

interface RideModelInterface {
    List<Ride> getAllRides(FilterConditions filterConditions);

    Ride getRide(String id);

    // Add the ride
    void addRide(Ride ride);

    void addPassengerToRide(String rideId, User user);

    // Update the student details
    void updateRide(String id,
                    String name,
                    String address,
                    String phone,
                    Date rideDate,
                    Map<String, User> passengers,
                    int numberOfPlaces,
                    String imageUrl, User creator);

    // Delete the ride
    void delete(Ride ride);
}

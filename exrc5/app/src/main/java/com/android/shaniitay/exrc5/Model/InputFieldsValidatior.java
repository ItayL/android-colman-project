package com.android.shaniitay.exrc5.Model;

import android.app.Activity;
import android.util.Patterns;
import android.widget.EditText;

import com.android.shaniitay.exrc5.ToastsUtils;

/**
 * Created by itay on 6/26/2017.
 */

public class InputFieldsValidatior {

    public static boolean validateFields(EditText numberOfPlacesEt, EditText sourceEt, EditText destinationEt, EditText phoneEt, Activity callingActivity) {

        if (numberOfPlacesEt.getText().toString().equals("")) {
            ToastsUtils.raiseToast(callingActivity, "Number Of Places Must Be Set!");
            return false;
        }
        if (sourceEt.getText().toString().equals("")) {
            ToastsUtils.raiseToast(callingActivity, "Source Must Be Set!");
            return false;
        }
        if (destinationEt.getText().toString().equals("")) {
            ToastsUtils.raiseToast(callingActivity, "Destination Must Be Set!");
            return false;
        }
        if (!Patterns.PHONE.matcher(phoneEt.getText()).matches() || phoneEt.getText().length() != 10) {
            ToastsUtils.raiseToast(callingActivity, "Phone number must contain 10 digits");
            return false;
        }

        return true;
    }

    private static boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}

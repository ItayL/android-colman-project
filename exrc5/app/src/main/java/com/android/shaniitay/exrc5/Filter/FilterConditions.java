package com.android.shaniitay.exrc5.Filter;

import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by Shani on 18/06/2017.
 */

public class FilterConditions implements Serializable{
    public String source;
    public String destination;
    public Calendar date;
    public String driver;
}

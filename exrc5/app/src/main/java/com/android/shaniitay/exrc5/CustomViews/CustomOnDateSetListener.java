package com.android.shaniitay.exrc5.CustomViews;

public interface CustomOnDateSetListener {
    void onDateSet(int year, int month, int dayOfMonth);
}

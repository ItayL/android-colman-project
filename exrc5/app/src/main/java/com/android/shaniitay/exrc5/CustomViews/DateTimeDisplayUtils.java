package com.android.shaniitay.exrc5.CustomViews;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by itay on 5/14/2017.
 */

public class DateTimeDisplayUtils {

    public static String getDisplayDate(Calendar date) {
        String displayMonth;
        String displayDayOfMonth;

        int displayMonthVal = date.get(Calendar.MONTH) + 1;
        if (displayMonthVal < 10) {
            displayMonth = "0" + displayMonthVal;
        }
        else {
            displayMonth = "" + displayMonthVal;
        }

        int displayDayOfMonthVal = date.get(Calendar.DAY_OF_MONTH);
        if (displayDayOfMonthVal < 10) {
            displayDayOfMonth = "0" + displayDayOfMonthVal;
        }
        else {
            displayDayOfMonth = "" + displayDayOfMonthVal;
        }

        return displayDayOfMonth + "/" + displayMonth + "/" + (date.get(Calendar.YEAR));
    }

    public static String getDisplayTime(int hour, int minute) {
        String displayHourStr;
        String displayMinute;
        String displayAmPm;

        Calendar timeCalendar = Calendar.getInstance();
        timeCalendar.set(Calendar.HOUR_OF_DAY, hour);
        int displayHour = timeCalendar.get(Calendar.HOUR);

        if (displayHour == 0) {
            displayHour = 12;
        }

        if (displayHour < 10) {
            displayHourStr = "0" + displayHour;
        }
        else {
            displayHourStr = "" + displayHour;
        }

        if (timeCalendar.get(Calendar.AM_PM) == Calendar.PM) {
            displayAmPm = "PM";
        }
        else {
            displayAmPm = "AM";
        }

        if (minute < 10) {
            displayMinute = "0" + minute;
        }
        else {
            displayMinute = "" + minute;
        }

        return displayHourStr + ":" + displayMinute + " " + displayAmPm;
    }
}

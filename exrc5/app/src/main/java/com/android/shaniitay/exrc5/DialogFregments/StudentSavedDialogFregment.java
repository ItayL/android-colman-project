package com.android.shaniitay.exrc5.DialogFregments;

import android.support.annotation.NonNull;

/**
 * Created by itay on 5/13/2017.
 */

public class StudentSavedDialogFregment extends StudentChangedDialogFregment {
    @NonNull
    @Override
    protected String getDialogMessage() {
        return "The student with the id '" + getSavedStudent().getId() + "' was saved";
    }
}
